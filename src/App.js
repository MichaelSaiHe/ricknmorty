import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CharacterDetail from "./Components/CharacterDetail";
import Home from "./Components/Home";

class App extends React.Component {
  render() {
    return (
      <Router>
      <div class="p-3 mb-2 bg-secondary text-white">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/profile" component={CharacterDetail} />
        </Switch>
      </div>
      </Router>
    );
  }
}
export default App;
