import React from "react";
import Characters from "./Characters";
import Container from "react-bootstrap/Container";
const baseUrl = "https://rickandmortyapi.com/api/character/";
//const baseUrl = "http://localhost:8080/api/character/";

class CharacterList extends React.Component {
  state = {
    characters: [],
    lastSearch: "",
    nextChars: ""
  };
  async componentDidMount() {
    try {
      const response = await fetch(baseUrl).then(resp => resp.json());
      let chars = response.results;

      this.setState({
        characters: chars,
        nextChars: response.info.next,
      });
    } catch (e) {
      console.error(e);
    }
  }

  async getMoreCharacters(){
    try{
      const response = await fetch(this.state.nextChars).then(resp => resp.json());
      let char = this.state.characters;
      char = char.push(...response.results)
      this.setState({
        character: char,
        nextChars: response.info.next
      })
  } catch (e){
    console.error(e);
  }}
  
   async componentDidUpdate() {
   try {
      document.addEventListener('scroll', this.trackScrolling)
    if (this.state.lastSearch !== this.props.searchWord){
      const response = await fetch(baseUrl+"?name="+this.props.searchWord).then(resp => resp.json());
    

    if(response.results !== undefined) {
      let chars = response.results;
      this.setState({
        characters : chars,
        lastSearch: this.props.searchWord,
        nextChars: response.info.next
      })
    } else{
      this.setState({
        characters: [],
        lastSearch: this.props.searchWord,
        nextChars: response.info.next
      })
    }
   }} catch (e){
    console.error(e);
 }}
 
 trackScrolling = () => {
   const wrappedEl = document.getElementById('charactersId');
   if (this.isBottom(wrappedEl)){
     document.removeEventListener('scroll', this.trackScrolling);
     this.getMoreCharacters();
   }
 }
 isBottom(el){
   return el.getBoundingClientRect().bottom <= window.innerHeight;
 }

 componentWillUnmount() {
   document.removeEventListener('scroll',this.trackScrolling);
 }

  render() {
    const characters = this.state.characters.map(character => {
      return <Characters character={character} key={character.id} />;
    });
    return (
      <div class="p-3 mb-2 bg-secondary text-white">
        <div className="container">
          <Container fluid>
            <div className="characters" id="charactersId">{characters}</div>
          </Container>
        </div>
      </div>
    );
  }
}

export default CharacterList;
