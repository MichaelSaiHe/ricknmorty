import React from "react";
import CharacterList from "./CharacterList";
import Image from "react-bootstrap/Image";
import banner from "../banner.jpg";

class SearchBar extends React.Component {
  getSearchValue = evt => {
    this.props.handleGetSearchValue(evt.target.value);
  };
  render() {
    return (
      <input
        className="searchbar"
        onChange={this.getSearchValue}
        placeholder="Enter a Character name..."
      ></input>
    );
  }
}
class Home extends React.Component {
  state = {
    searchWord: ""
  };

  constructor(props) {
    super(props);
    this.getSearchValue = this.getSearchValue.bind(this);
  }

  getSearchValue(value) {
    try {
      this.setState({
        searchWord: value
      });
    } catch (e) {
      console.error(e);
    }
  }
  
  render() {
    return (
      <div>
        <div className="top-div">
          <header className="header">
            <Image src={banner} fluid />
          </header>
          <div className="searchbar-div">
            <SearchBar handleGetSearchValue={this.getSearchValue}></SearchBar>
          </div>
          <body>
          <CharacterList searchWord={this.state.searchWord}></CharacterList>
          </body>
        </div>
      </div>
    );
  }
}
export default Home;
