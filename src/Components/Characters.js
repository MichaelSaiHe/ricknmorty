import React from "react";
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

const Characters = props => {
  return (
    <div className="cards-container">
      <CardDeck>
        <Card className="card" style={{ width: "18rem" }}>
          <Card.Img variant="top" src={props.character.image} alt="img" />
          <div className="card-body">
            <Card.Body>
              <Card.Title>{props.character.name}</Card.Title>
              <Card.Text>Status: {props.character.gender}</Card.Text>
              <Card.Text>Species: {props.character.species}</Card.Text>
              <Card.Text>Gender: {props.character.gender}</Card.Text>
              <Card.Text>Origin: {props.character.origin.name}</Card.Text>
              <Card.Text>
                Last location: {props.character.location.name}
              </Card.Text>
              <Link to={{
                pathname: "/profile/" + props.character.id,
                state: {
                  character: props.character
                }
              }}><Button variant="info">Details</Button></Link>
            </Card.Body>
          </div>
        </Card>
      </CardDeck>
    </div>
  );
};

export default Characters;
